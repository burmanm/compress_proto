package compression

import (
	"time"
)

const (
	defaultBlockSize = 24 * time.Hour
)

// TODO How could we calculate the correct beginning and end key if someone makes the blockSize
// larger than originally? Smaller is fine, it does not pose a problem

// BlockSize returns a given blocksize for the metric or the default
func BlockSize(metric string) time.Duration {
	// Implement keyCache lookup here or something.. and should include tenant & metricType of course
	return defaultBlockSize
}
