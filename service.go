package compression

import (
	"fmt"
	"time"
)

type Service struct {
	c *CassandraConnection
}

func NewService(c *CassandraConnection) *Service {
	return &Service{
		c: c,
	}
}

// StoreData Compresses the data and then writes it to the Cassandra
func (s *Service) StoreData(tenantId string, metricType uint8, metric string, datapoints []Datapoint) error {

	// fmt.Printf("%v\n", datapoints[0].Timestamp)

	compressAndSend := func(datapoints []Datapoint) error {
		b, err := CompressPackage(datapoints)
		if err != nil {
			return err
		}

		// Send to goroutine for parallelism
		err = s.c.InsertData(tenantId, metricType, metric, datapoints[0].Timestamp, b)
		if err != nil {
			return err
		}

		return nil
	}

	startCount := 0
	prevTimestamp := ToUnixMilli(datapoints[0].Timestamp.Truncate(BlockSize(metric)))
	for i, d := range datapoints {
		thisTimestamp := ToUnixMilli(d.Timestamp.Truncate(BlockSize(metric)))
		if prevTimestamp != thisTimestamp {
			// Store previous and start a new one..
			// fmt.Printf("Would store segment of %d to %d\n", startCount, i-1)

			compressAndSend(datapoints[startCount:i])

			startCount = i
			prevTimestamp = thisTimestamp
		}
	}
	// fmt.Printf("Would store segment of %d to %d\n", startCount, len(datapoints)-1)
	compressAndSend(datapoints[startCount:len(datapoints)])

	return nil
}

// ReadData reads from Cassandra, decompresses it and filters out the datapoints that do not match the criteria
func (s *Service) ReadData(tenantId string, metricType uint8, metric string, startTime time.Time, endTime time.Time) ([]*Datapoint, error) {

	compressedData, err := s.c.ReadData(tenantId, metricType, metric, startTime, endTime)
	if err != nil {
		return nil, err
	}

	filteredDatapoints := make([]*Datapoint, 1024) // Will autogrow.. can't predict the size yet

	for _, cData := range compressedData {
		uncompressedData, err := DecompressPackage(cData)
		if err != nil {
			return nil, err
		}

		for _, datapoint := range uncompressedData {
			// Not probably the most optimized solution..
			if !datapoint.Timestamp.Before(startTime) && !datapoint.Timestamp.After(endTime) {
				filteredDatapoints = append(filteredDatapoints, datapoint)
			}
		}
	}

	return filteredDatapoints, nil
}

func (s *Service) TransferData() error {
	pKeys, err := s.c.PartitionKeys()
	if err != nil {
		return err
	}
	fmt.Printf("Found %d partition keys\n", len(pKeys))

	for _, pKey := range pKeys {
		dpoints := s.c.ReadPlainData(pKey)
		if len(dpoints) > 0 {
			s.StoreData(pKey.TenantId, pKey.MetricType, pKey.Metric, dpoints)
		}
		// fmt.Printf("Received %d datapoints for key %v\n", len(dpoints), pKey)
	}

	return nil
}
