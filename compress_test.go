package compression

import (
	"fmt"
	"math/rand"
	"strconv"
	"testing"
	"time"
)

func getDatapoints() []Datapoint {

	r := rand.New(rand.NewSource(time.Now().UnixNano()))

	prefix := r.Int()

	points := 1000
	metrics := 1

	ts := time.Now()

	data := make([]Datapoint, 0, points*metrics)

	for j := 0; j < (points * metrics); j += points {
		for k := 0; k < points; k++ {
			ts = ts.Add(10 * time.Second)
			data = append(data, Datapoint{
				Timestamp: ts,
				Value:     float64(k * prefix),
			})
		}
	}

	return data
}

// Needs to load data to the data table first..
// func TestTransferredData(t *testing.T) {
// 	c, err := NewCassandraConnection(false)
// 	assert.NoError(t, err)

// 	s := NewService(c)
// 	err = s.TransferData()
// 	assert.NoError(t, err)

// 	c.Close()
// }

func insertBenchmarkData(tenant string, id string, ts time.Time, c *CassandraConnection) error {
	data := make([]Datapoint, 0, 1024)

	for k := 0; k < 1024; k++ {
		ts = ts.Add(10 * time.Second)
		data = append(data, Datapoint{
			Timestamp: ts,
			Value:     float64(k),
		})
	}

	// fmt.Printf("Last timestamp was: %v\n", data[len(data)-1].Timestamp)

	// Now, write the data to both tables
	err := c.InsertPlainData(tenant, 0, id, data)
	if err != nil {
		return err
	}

	bytes, err := CompressPackage(data)
	if err != nil {
		return err
	}

	err = c.InsertData(tenant, 0, id, data[0].Timestamp, bytes)
	return err
}

func BenchmarkCompressedPartialReadSpeed(b *testing.B) {

	b.SetParallelism(1)

	c, err := NewCassandraConnection(true)
	if err != nil {
		b.FailNow()
	}

	s := NewService(c)

	ts := time.Date(2016, time.July, 26, 0, 0, 0, 0, time.UTC)
	tenant := strconv.Itoa(int(ts.Unix()))
	id := fmt.Sprintf("test.%s", tenant)

	err = insertBenchmarkData(tenant, id, ts, c)
	if err != nil {
		b.FailNow()
	}

	tsEnd := ts.Add(time.Hour) // This should read approximately 360 datapoints

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		dpoints, err := s.ReadData(tenant, 0, id, ts, tsEnd)
		if err != nil {
			b.FailNow()
		}
		if len(dpoints) < 300 {
			b.FailNow()
		}
	}

	b.StopTimer()
	c.Close()
}

func BenchmarkUncompressedPartialReadSpeed(b *testing.B) {
	c, err := NewCassandraConnection(true)
	if err != nil {
		b.FailNow()
	}

	ts := time.Date(2016, time.July, 26, 0, 0, 0, 0, time.UTC)
	tenant := strconv.Itoa(int(ts.Unix()))
	id := fmt.Sprintf("test.%s", tenant)

	err = insertBenchmarkData(tenant, id, ts, c)
	if err != nil {
		b.FailNow()
	}

	tsEnd := ts.Add(time.Hour) // This should read approximately 360 datapoints

	// fmt.Printf("Should request until %v\n", tsEnd)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		dpoints, err := c.ReadTimeDiff(tenant, 0, id, ts, tsEnd)
		// fmt.Printf("Received %d datapoints\n", len(dpoints))
		if err != nil || len(dpoints) < 300 {
			b.FailNow()
		}
	}
	b.StopTimer()
	c.Close()
}

func TestCompressedCassandraAndBack(t *testing.T) {

	// tenantId := "t1"
	// metric := "test.compress.1"
	// metricType := uint8(1)

	// c, err := NewCassandraConnection(false)
	// assert.NoError(t, err)

	// s := NewService(c)

	// data := getDatapoints()

	// err = s.StoreData(tenantId, metricType, metric, data)
	// assert.NoError(t, err)

	// b, err := CompressPackage(data)
	// assert.NoError(t, err)

	// mD := MetricData{
	// 	TenantId:   "t1",
	// 	MetricType: uint8(1),
	// 	Metric:     "test.compress.1",
	// 	Time:       data[0].Timestamp,
	// 	Chunks:     b,
	// }

	// err = c.InsertData(mD)
	// assert.NoError(t, err)

	// cb, err := s.ReadData(tenantId, metricType, metric, data[0].Timestamp, data[len(data)/2].Timestamp)
	// assert.NoError(t, err)

	// err = s.TransferData()
	// assert.NoError(t, err)

	// assert.True(t, len(cb) < len(data))

	// cb, err := c.ReadData("t1", uint8(1), "test.compress.1", data[0].Timestamp, data[len(data)-1].Timestamp)
	// assert.NoError(t, err)

	// assert.Equal(t, 1, len(cb))

	// for _, cbytes := range cb {
	// 	dps, err := DecompressPackage(cbytes)
	// 	assert.NoError(t, err)

	// 	fmt.Printf("Uncompressed %d chunks\n", len(dps))

	// 	assert.Equal(t, len(data), len(dps))
	// 	assert.Equal(t, data[0].Timestamp, dps[0].Timestamp)
	// }
}
