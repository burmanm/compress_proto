package compression

import "time"

type Datapoint struct {
	Timestamp time.Time
	Value     float64
}

type MetricData struct {
	TenantId   string
	MetricType uint8
	Metric     string
	Time       time.Time
	Chunks     []byte
}

// Header is the beginning of the compressed chunk, equal to all compression methods
type Header struct {
	CompressionType uint8
	ChunksAmount    uint32 // Do we need so high number?
	FirstTimestamp  uint64
	LastTimestamp   uint64 // We should use offset of the first value, reduce bytes
}

type PartitionKey struct {
	// PRIMARY KEY ((tenant_id, type, metric, dpart), time)
	TenantId   string
	MetricType uint8
	Metric     string
	Dpart      int64
	// Time       time.Time
}
