package compression

import (
	"fmt"
	"time"

	"github.com/gocql/gocql"
	"github.com/hawkular/hawkular-client-go/metrics"
)

// CassandraConnection stores internal Cassandra connection related information
type CassandraConnection struct {
	session *gocql.Session
}

// NewCassandraConnection returns a new connection the localhost running Cassandra in hawkular keyspace
func NewCassandraConnection(reCreate bool) (*CassandraConnection, error) {
	cluster := gocql.NewCluster("127.0.0.1")
	cluster.Consistency = gocql.One
	cluster.ProtoVersion = 4

	if reCreate {
		stmts := make([]string, 0, 2)
		stmts = append(stmts, "DROP KEYSPACE IF EXISTS hawkular_compress")
		stmts = append(stmts, "CREATE KEYSPACE hawkular_compress WITH replication = {'class': 'SimpleStrategy', 'replication_factor': 1}")
		cluster.Keyspace = "system"
		session, err := cluster.CreateSession()
		if err != nil {
			fmt.Printf("Failed to connect Cassandra to reCreate keyspace\n")
			return nil, err
		}

		for _, stmt := range stmts {
			err := session.Query(stmt).Exec()
			if err != nil {
				fmt.Printf("Failed to execute query: %s\n", stmt)
				return nil, err
			}
		}
		session.Close()
	}

	cluster.Keyspace = "hawkular_compress"

	session, err := cluster.CreateSession()
	if err != nil {
		return nil, err
	}

	c := &CassandraConnection{
		session: session,
	}

	if reCreate {
		err = c.CreateTables()
		if err != nil {
			return nil, err
		}
	}

	return c, nil
}

func (c *CassandraConnection) CreateTables() error {
	stmts := make([]string, 0, 2)

	// stmts = append(stmts, "DROP TABLE data")
	// stmts = append(stmts, "DROP TABLE data_old")

	stmts = append(stmts, `CREATE TABLE data (
    tenant_id text,
    type tinyint,
    metric text,
    dpart bigint,
    time timeuuid,
    data_retention int static,
    n_value double,
    availability blob,
    l_value bigint,
    PRIMARY KEY ((tenant_id, type, metric, dpart), time)
) WITH CLUSTERING ORDER BY (time DESC)`)

	stmts = append(stmts, `CREATE TABLE data_old (
    tenant_id text,
    type tinyint,
    metric text,
    time timestamp,
    chunks blob,
    PRIMARY KEY ((tenant_id, type, metric), time)
) WITH CLUSTERING ORDER BY (time DESC)`)

	//  AND compression = { 'class' : 'DeflateCompressor', 'chunk_length_in_kb' : 64 }

	for _, stmt := range stmts {
		err := c.session.Query(stmt).Exec()
		if err != nil {
			fmt.Printf("Failed to execute query: %s\n", stmt)
			return err
		}
	}

	return nil
}

// InsertData stores compressed blocks to the new long term table
func (c *CassandraConnection) InsertData(tenantId string, metricType uint8, metric string, startTime time.Time, chunks []byte) error {
	keyTimestamp := startTime.Truncate(BlockSize(metric))

	// fmt.Printf("Inserting data to key %d\n", keyTimestamp)
	stmt := "INSERT INTO data_old (tenant_id, type, metric, time, chunks) VALUES (?, ?, ?, ?, ?)"

	err := c.session.Query(stmt, tenantId, metricType, metric, keyTimestamp, chunks).Exec()
	if err != nil {
		return err
	}

	return nil
}

// InsertPlainData stores datapoints to the current table format
func (c *CassandraConnection) InsertPlainData(tenantId string, metricType uint8, metric string, datapoints []Datapoint) error {
	stmt := "INSERT INTO data (tenant_id, type, metric, dpart, time, n_value) VALUES (?, ?, ?, ?, ?, ?)"

	for _, dp := range datapoints {
		val, err := metrics.ConvertToFloat64(dp.Value)

		if err != nil {
			return err
		}
		err = c.session.Query(stmt, tenantId, metricType, metric, 0, gocql.UUIDFromTime(dp.Timestamp), val).Exec()
		if err != nil {
			return err
		}
	}

	return nil
}

func (c *CassandraConnection) ReadTimeDiff(tenantId string, metricType uint8, metric string, startTime time.Time, endTime time.Time) ([]Datapoint, error) {
	stmt := "SELECT n_value, time FROM data WHERE tenant_id = ? AND type = ? AND metric = ? AND dpart = ? AND time >= ? AND time <= ?"

	// startTimestamp := ToUnixMilli(startTime)
	// endTimestamp := ToUnixMilli(endTime)

	iter := c.session.Query(stmt, tenantId, 0, metric, 0, gocql.UUIDFromTime(startTime), gocql.UUIDFromTime(endTime)).Iter()

	datapoints := make([]Datapoint, 0, iter.NumRows())
	var timestamp time.Time
	var n_value float64

	for iter.Scan(&n_value, &timestamp) {
		datapoints = append(datapoints, Datapoint{
			Timestamp: timestamp,
			Value:     n_value,
		})
	}

	err := iter.Close()

	return datapoints, err
}

func (c *CassandraConnection) ReadPlainData(pKey PartitionKey) []Datapoint {
	stmt := "SELECT type, n_value, time, l_value FROM data WHERE tenant_id = ? AND type = ? AND metric = ? AND dpart = ?"
	iter := c.session.Query(stmt, pKey.TenantId, pKey.MetricType, pKey.Metric, pKey.Dpart).Iter()

	datapoints := make([]Datapoint, 0, iter.NumRows())

	var typ uint8
	var timestamp time.Time
	var n_value float64
	var l_value int64
	for iter.Scan(&typ, &n_value, &timestamp, &l_value) {
		if typ == 0 {
			datapoints = append(datapoints, Datapoint{
				Timestamp: timestamp,
				Value:     n_value,
			})
		} else if typ == 2 {
			val, err := metrics.ConvertToFloat64(l_value)
			if err != nil {
				fmt.Printf("Failed to convert to float64\n")
			}
			datapoints = append(datapoints, Datapoint{
				Timestamp: timestamp,
				Value:     val,
			})
		}
	}
	return datapoints
}

func (c *CassandraConnection) PartitionKeys() ([]PartitionKey, error) {
	stmt := "SELECT DISTINCT tenant_id, type, metric, dpart FROM data"

	iter := c.session.Query(stmt).Iter()

	pKeys := make([]PartitionKey, 0, iter.NumRows())

	var tenant_id, metric string
	var typ uint8
	var dpart int64
	for iter.Scan(&tenant_id, &typ, &metric, &dpart) {
		pKeys = append(pKeys, PartitionKey{
			TenantId:   tenant_id,
			MetricType: typ,
			Metric:     metric,
			Dpart:      dpart,
		})
	}

	err := iter.Close()
	if err != nil {
		return nil, err
	}

	return pKeys, nil
}

// ReadData reads compressed blocks from a new long term table
func (c *CassandraConnection) ReadData(tenantId string, metricType uint8, metric string, startTime time.Time, endTime time.Time) ([][]byte, error) {

	startTimestamp := ToUnixMilli(startTime.Truncate(BlockSize(metric)))
	endTimestamp := ToUnixMilli(endTime.Round(BlockSize(metric)))

	stmt := "SELECT chunks FROM data_old WHERE tenant_id = ? AND metric_type = ? AND metric = ? AND time >= ? AND time <= ?"

	var chunk []byte
	query := c.session.Query(stmt, tenantId, metricType, metric, startTimestamp, endTimestamp)

	// fmt.Printf("Query: %v\n", query)
	iter := query.Iter()

	// fmt.Printf("Received %d rows from the Cassandra, query: %d -> %d\n", iter.NumRows(), startTimestamp, endTimestamp)

	results := make([][]byte, 0, iter.NumRows())
	for iter.Scan(&chunk) {
		results = append(results, chunk)
	}
	return results, nil
}

// func (c *CassandraConnection) LoadPlainData() error {
// 	stmt := "COPY data (tenant_id, type, metric, dpart, time, n_value, l_value) FROM '/home/michael/projects/git/compression_test/testdata/myexport.csv'"

// 	fmt.Printf("Starting to load the data from testdata\n")

// 	err := c.session.Query(stmt).Exec()

// 	fmt.Printf("Finished loading the data\n")
// 	return err
// }

// Close closes the connections and frees resources
func (c *CassandraConnection) Close() {
	c.session.Close()
}

func ToUnixMilli(t time.Time) int64 {
	return t.UnixNano() / (int64(time.Millisecond) / int64(time.Nanosecond))
}
