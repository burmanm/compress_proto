package compression

import (
	"time"

	tsz "github.com/dgryski/go-tsz"
)

func DecompressPackage(chunks []byte) ([]*Datapoint, error) {
	iter, err := tsz.NewIterator(chunks)
	if err != nil {
		return nil, err
	}

	dp := make([]*Datapoint, 0, 1024) // Current implementation has no chunk-amount

	for iter.Next() {
		ts, value := iter.Values()

		if err != nil {
			return nil, err
		}
		dts := time.Unix(int64(ts), 0)
		d := &Datapoint{
			Timestamp: dts,
			Value:     value,
		}

		dp = append(dp, d)
	}

	return dp, nil

}

func CompressPackage(datapoints []Datapoint) ([]byte, error) {

	// fmt.Printf("Compressing %d datapoints\n", len(datapoints))
	// fmt.Printf("Timestamp #1: %v\n", datapoints[0].Timestamp)

	startTime := uint32(datapoints[0].Timestamp.Unix()) // We should store uint64, but for now..
	s := tsz.New(startTime)

	for _, d := range datapoints {
		ts := uint32(d.Timestamp.Unix())
		s.Push(ts, d.Value)
	}

	s.Finish()

	return s.Bytes(), nil
}
